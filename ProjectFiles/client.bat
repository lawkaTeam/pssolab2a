::the client's codebase URL
SET myHTTPserverIP=192.168.43.113


::the client's codebase port
SET myHTTPserverPort=8080

SET RMIserverIP=192.168.43.140

REM SET LOG=-Dsun.rmi.loader.logLevel=SILENT
SET LOG=-Dsun.rmi.loader.logLevel=BRIEF

del /S client\*.class
del /S server\*.class
javac client/ComputePi.java
javac client/ComputePrimaryNumbers.java

start /B hfs.exe client
@echo.
@echo Wait until HFS is ready before starting ComputePi!
@echo.
@pause

java -Djava.rmi.server.codebase=http://%myHTTPserverIP%:%myHTTPserverPort%/ -Djava.security.policy=java.policy -Djava.rmi.server.useCodebaseOnly=false %LOG% client.ComputePi %RMIserverIP% 10
java -Djava.rmi.server.codebase=http://%myHTTPserverIP%:%myHTTPserverPort%/ -Djava.security.policy=java.policy -Djava.rmi.server.useCodebaseOnly=false %LOG% client.ComputePrimaryNumbers %RMIserverIP% 20002
