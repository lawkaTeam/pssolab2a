package clientTest;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import client.PrimaryNumbers;

public class PrimaryNumbersTest<BigDecimal> {

	@Test
	public void testComputePrimaryNumbers() {			
		assertEquals("2 3 5 7 11 ", PrimaryNumbers.generatePrimaryNumbersString(5));
		assertEquals("2 3 5 7 11 13 ", PrimaryNumbers.generatePrimaryNumbersString(6));
	}

}
