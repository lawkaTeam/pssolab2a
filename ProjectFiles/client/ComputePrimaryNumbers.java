package client;

import java.math.BigDecimal;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.util.List;
import server.Compute;

public class ComputePrimaryNumbers {

	public static void main(String[] args) {
		if (System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager());
        try {
        	System.out.print("**********  TEST PRIMARY NUMBERS   ***************");
            String name = "Compute";
            Registry registry = LocateRegistry.getRegistry(args[0]);
            Compute comp = (Compute) registry.lookup(name);
            PrimaryNumbers taskPrimaryNumbers = new PrimaryNumbers(Integer.parseInt(args[1]));
            List<BigDecimal> primaryNumbersList = comp.executeTask(taskPrimaryNumbers);
            for (BigDecimal primaryNo : primaryNumbersList) {
            	System.out.print(primaryNo);
            	System.out.print(" ");
            }
        } catch (Exception e) {
            System.err.println("Compute exception:");
            e.printStackTrace();
        }

	}

}
