package client;

import server.Task;

import java.awt.print.Printable;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrimaryNumbers implements Task<List<BigDecimal>>, Serializable {

	private final int numbersCount;

	private static List<BigDecimal> cachedList = new ArrayList<>();

	public PrimaryNumbers(int numbersCount) {
		this.numbersCount = numbersCount;
	}

	public List<BigDecimal> execute() {
		return computePrimaryNumbers(numbersCount);
	}

	public static List<BigDecimal> computePrimaryNumbers(int numbersCount) {
		List<BigDecimal> primeNumbers;
		
		primeNumbers = generateCachedPrimaryNumbers(numbersCount);		
		primeNumbers = generateFreshPrimaryNumbers(primeNumbers, numbersCount);		

		return primeNumbers;
	}

	private static List<BigDecimal> generateCachedPrimaryNumbers(int numbersCount) {
		List<BigDecimal> primeNumbers;
		primeNumbers = new ArrayList<>();
		int index = 0;
		for (; index < cachedList.size(); index++) {
			if (primeNumbers.size() == numbersCount) {
				break;
			}
			primeNumbers.add(cachedList.get(index));
		}
		return primeNumbers;
	}

	public static String generatePrimaryNumbersString(int numbersCount) {
		StringBuilder sb = new StringBuilder();
		for (BigDecimal value : computePrimaryNumbers(numbersCount)) {
			sb.append(value.toString());
			sb.append(" ");
		}
		return sb.toString();
	}

	private static List<BigDecimal> generateFreshPrimaryNumbers(List<BigDecimal> primeNumbers, int numbersCount) {
		List<BigDecimal> extendedPrimeNumbers = new ArrayList<>(primeNumbers);
		int number = 0;
		
		if (isListAlreadyFilledWithCachedValues(extendedPrimeNumbers)) {
			number = getLastPrimeNumber(extendedPrimeNumbers);
			number++;
		}
		
		while (extendedPrimeNumbers.size() != numbersCount) {
			if (isPrime(number)) {
				extendedPrimeNumbers.add(BigDecimal.valueOf(number));
				cachedList.add(BigDecimal.valueOf(number));
			}
			number++;
		}
		return extendedPrimeNumbers;
	}

	private static Integer getLastPrimeNumber(List<BigDecimal> extendedPrimeNumbers) {
		return Integer.valueOf(extendedPrimeNumbers.get(extendedPrimeNumbers.size() - 1).toString());
	}

	private static boolean isListAlreadyFilledWithCachedValues(List<BigDecimal> extendedPrimeNumbers) {
		return extendedPrimeNumbers.size() != 0;
	}

	private static boolean isPrime(int number) {
		if (number < 2) {
			return false;
		}
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}
}